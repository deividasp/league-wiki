package org.bitbucket.deividasp.leaguewiki.repository.ability;

import org.bitbucket.deividasp.leaguewiki.model.api.ability.Ability;

import java.util.List;

public interface IAbilityRepository {

    List<Ability> getAll();

    Ability get(long id);
    Ability get(String name);

    void submit(Ability ability);

    List<Ability> getSubmitted();

    void accept(long id);
    void remove(long id);

}
