package org.bitbucket.deividasp.leaguewiki.repository.champion;

import org.bitbucket.deividasp.leaguewiki.model.api.champion.Champion;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.ChampionRole;

import java.util.List;

public interface IChampionRepository {

    List<Champion> getAll();

    Champion get(long id);
    Champion get(String name);

    List<ChampionRole> getRoles();

    List<Champion> getByRole(String roleName);
    List<Champion> getByPrimaryRole(String roleName);
    List<Champion> getBySecondaryRole(String roleName);

	List<String> getNames();

    void submit(Champion champion);

    List<Champion> getSubmitted();

    void accept(long id);
    void remove(long id);

}