package org.bitbucket.deividasp.leaguewiki.repository.ability;

import org.bitbucket.deividasp.leaguewiki.model.api.APIEntryStatus;
import org.bitbucket.deividasp.leaguewiki.model.api.ability.Ability;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.List;

@Repository
@Transactional
public class AbilityRepository implements IAbilityRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Ability> getAll() {
        return entityManager.createQuery("from Ability where status = :status", Ability.class)
                .setParameter("status", APIEntryStatus.ACCEPTED)
                .getResultList();
    }

    @Override
    public Ability get(long id) {
        return entityManager.createQuery("from Ability where id = :id", Ability.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Ability get(String name) {
        return entityManager.createQuery("from Ability where name = :name", Ability.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public void submit(Ability ability) {
        entityManager.persist(ability);
    }

    @Override
    public List<Ability> getSubmitted() {
        return entityManager.createQuery("from Ability where status = :status", Ability.class)
                .setParameter("status", APIEntryStatus.SUBMITTED)
                .getResultList();
    }

    @Override
    public void accept(long id) {
        Ability ability = entityManager.createQuery("from Ability where id = :id", Ability.class)
                .setParameter("id", id)
                .getSingleResult();
        ability.setStatus(APIEntryStatus.ACCEPTED);

        entityManager.merge(ability);
    }

    @Override
    public void remove(long id) {
        Ability ability = entityManager.createQuery("from Ability where id = :id", Ability.class)
                .setParameter("id", id)
                .getSingleResult();

        entityManager.remove(ability);
    }

}