package org.bitbucket.deividasp.leaguewiki.repository.item;

import org.bitbucket.deividasp.leaguewiki.model.api.APIEntryStatus;
import org.bitbucket.deividasp.leaguewiki.model.api.item.Item;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.List;

@Repository
@Transactional
public class ItemRepository implements IItemRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Item> getAll() {
        return entityManager.createQuery("from Item where status = :status", Item.class)
                .setParameter("status", APIEntryStatus.ACCEPTED)
                .getResultList();
    }

    @Override
    public Item get(long id) {
        return entityManager.createQuery("from Item where id = :id", Item.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Item get(String name) {
        return entityManager.createQuery("from Item where name = :name", Item.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public void submit(Item item) {
        entityManager.persist(item);
    }

    @Override
    public List<Item> getSubmitted() {
        return entityManager.createQuery("from Item where status = :status", Item.class)
                .setParameter("status", APIEntryStatus.SUBMITTED)
                .getResultList();
    }

    @Override
    public void accept(long id) {
        Item item = entityManager.createQuery("from Item where id = :id", Item.class)
                .setParameter("id", id)
                .getSingleResult();
        item.setStatus(APIEntryStatus.ACCEPTED);

        entityManager.merge(item);
    }

    @Override
    public void remove(long id) {
        Item item = entityManager.createQuery("from Item where id = :id", Item.class)
                .setParameter("id", id)
                .getSingleResult();

        entityManager.remove(item);
    }
}
