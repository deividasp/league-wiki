package org.bitbucket.deividasp.leaguewiki.repository.item;

import org.bitbucket.deividasp.leaguewiki.model.api.item.Item;

import java.util.List;

public interface IItemRepository {

    List<Item> getAll();

    Item get(long id);
    Item get(String name);

    void submit(Item item);

    List<Item> getSubmitted();

    void accept(long id);
    void remove(long id);

}