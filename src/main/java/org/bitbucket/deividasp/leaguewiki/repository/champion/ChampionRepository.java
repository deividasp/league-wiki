package org.bitbucket.deividasp.leaguewiki.repository.champion;

import org.bitbucket.deividasp.leaguewiki.model.api.APIEntryStatus;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.Champion;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.ChampionRole;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Transactional
public class ChampionRepository implements IChampionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Champion> getAll() {
        return entityManager.createQuery("from Champion where status = :status", Champion.class)
                .setParameter("status", APIEntryStatus.ACCEPTED)
                .getResultList();
    }

    @Override
    public Champion get(long id) {
        return entityManager.createQuery("from Champion where id = :id", Champion.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Champion get(String name) {
        return entityManager.createQuery("from Champion where name = :name", Champion.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public List<ChampionRole> getRoles() {
        return Stream.of(ChampionRole.values()).collect(Collectors.toList());
    }

    @Override
    public List<Champion> getByRole(String roleName) {
        return entityManager.createQuery("from Champion where (primaryRole = :role or secondaryRole = :role) and status = :status", Champion.class)
                .setParameter("role", ChampionRole.valueOf(roleName.toUpperCase()))
                .setParameter("status", APIEntryStatus.ACCEPTED)
                .getResultList();
    }

    @Override
    public List<Champion> getByPrimaryRole(String roleName) {
        return entityManager.createQuery("from Champion where primaryRole = :role and status = :status", Champion.class)
                .setParameter("role", ChampionRole.valueOf(roleName.toUpperCase()))
                .setParameter("status", APIEntryStatus.ACCEPTED)
                .getResultList();
    }

    @Override
    public List<Champion> getBySecondaryRole(String roleName) {
        return entityManager.createQuery("from Champion where secondaryRole = :role and status = :status", Champion.class)
                .setParameter("role", ChampionRole.valueOf(roleName.toUpperCase()))
                .setParameter("status", APIEntryStatus.ACCEPTED)
                .getResultList();
    }

	@Override
	public List<String> getNames() {
		List<String> championNames = new ArrayList<>();
		getAll().forEach(c -> championNames.add(c.getName()));

		return championNames;
	}

    @Override
    public void submit(Champion champion) {
        entityManager.persist(champion);
    }

    @Override
    public List<Champion> getSubmitted() {
        return entityManager.createQuery("from Champion where status = :status", Champion.class)
                .setParameter("status", APIEntryStatus.SUBMITTED)
                .getResultList();
    }

    @Override
    public void accept(long id) {
        Champion champion = entityManager.createQuery("from Champion where id = :id", Champion.class)
                .setParameter("id", id)
                .getSingleResult();
        champion.setStatus(APIEntryStatus.ACCEPTED);

        entityManager.merge(champion);
    }
    
    @Override
    public void remove(long id) {
        Champion champion = entityManager.createQuery("from Champion where id = :id", Champion.class)
                .setParameter("id", id)
                .getSingleResult();

        entityManager.remove(champion);
    }

}