package org.bitbucket.deividasp.leaguewiki.model.api.item;

import org.bitbucket.deividasp.leaguewiki.model.api.APIEntry;
import org.bitbucket.deividasp.leaguewiki.model.api.item.effect.UniqueEffect;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "items")
public class Item extends APIEntry {

    @Id
    @GeneratedValue
    private long id;

	@NotNull
	private String name;

    @NotNull
    private String cost;

    @NotNull
    @Column(columnDefinition = "LONGBLOB")
    private Bonus[] bonuses;

    @NotNull
    @Column(columnDefinition = "LONGBLOB")
    private UniqueEffect[] effects;

    protected Item() {

    }

    public long getId() {
        return id;
    }

    public String getName() {
    	return name;
    }

    public String getCost() {
        return cost;
    }

    public Bonus[] getBonuses() {
        return bonuses;
    }

    public UniqueEffect[] getEffects() {
        return effects;
    }

}