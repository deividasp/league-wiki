package org.bitbucket.deividasp.leaguewiki.model.api.ability;

import org.bitbucket.deividasp.leaguewiki.model.api.APIEntry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "abilities")
public class Ability extends APIEntry {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    private long championId;

    @NotNull
    private String name;

    @NotNull
    private String cost;

    @NotNull
    private String castRange;

    @NotNull
    @Column(length = 10000)
    private String description;

    protected Ability() {

    }

    public long getId() {
        return id;
    }

    public long getChampionId() {
        return championId;
    }

    public String getName() {
        return name;
    }

    public String getCost() {
        return cost;
    }

    public String getCastRange() {
        return castRange;
    }

    public String getDescription() {
        return description;
    }

}