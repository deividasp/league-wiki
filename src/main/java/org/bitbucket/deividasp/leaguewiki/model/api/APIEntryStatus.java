package org.bitbucket.deividasp.leaguewiki.model.api;

public enum APIEntryStatus {

    ACCEPTED,
    SUBMITTED

}