package org.bitbucket.deividasp.leaguewiki.model.api.item.effect;

public enum UniqueEffectType {

    AURA,
    ACTIVE,
    PASSIVE,

}