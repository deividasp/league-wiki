package org.bitbucket.deividasp.leaguewiki.model.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class APIEntry {

    private static final APIEntryStatus DEFAULT_STATUS = APIEntryStatus.SUBMITTED;

    private APIEntryStatus status = DEFAULT_STATUS;

    public void setStatus(APIEntryStatus status) {
        this.status = status;
    }

    @JsonIgnore
    public boolean isAccepted() {
        return status == APIEntryStatus.ACCEPTED;
    }

    @JsonIgnore
    public boolean isSubmitted() {
        return status == APIEntryStatus.SUBMITTED;
    }

}