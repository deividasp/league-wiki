package org.bitbucket.deividasp.leaguewiki.model.api.item;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "bonuses")
public class Bonus implements Serializable {

    @Id
    @GeneratedValue
    @JsonIgnore
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String bonus;

    protected Bonus() {

    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBonus() {
        return bonus;
    }

}