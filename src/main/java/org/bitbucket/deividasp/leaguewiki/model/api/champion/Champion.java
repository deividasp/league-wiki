package org.bitbucket.deividasp.leaguewiki.model.api.champion;

import org.bitbucket.deividasp.leaguewiki.model.api.APIEntry;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "champions")
public class Champion extends APIEntry {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    private String name;

    @NotNull
    @Column(length = 10000)
    private String description;

    @NotNull
    @Column(columnDefinition = "LONGBLOB")
    private BaseStat[] baseStats;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ChampionRole primaryRole;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ChampionRole secondaryRole;

    protected Champion() {

    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BaseStat[] getBaseStats() {
        return baseStats;
    }

    public ChampionRole getPrimaryRole() {
        return primaryRole;
    }

    public ChampionRole getSecondaryRole() {
        return secondaryRole;
    }

}