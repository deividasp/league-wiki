package org.bitbucket.deividasp.leaguewiki.model.api.item.effect;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "unique_effects")
public class UniqueEffect implements Serializable {

    @Id
    @GeneratedValue
    @JsonIgnore
    private long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private UniqueEffectType type;

    @NotNull
    private String description;

    protected UniqueEffect() {

    }

    public long getId() {
        return id;
    }

    public UniqueEffectType getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

}