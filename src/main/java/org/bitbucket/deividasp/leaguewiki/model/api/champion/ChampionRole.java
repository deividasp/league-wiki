package org.bitbucket.deividasp.leaguewiki.model.api.champion;

public enum ChampionRole {

    TANK,
    MAGE,
    FIGHTER,
    SUPPORT,
    ASSASSIN,
    MARKSMAN

}