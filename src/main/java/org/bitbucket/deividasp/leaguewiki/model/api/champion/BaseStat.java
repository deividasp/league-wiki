package org.bitbucket.deividasp.leaguewiki.model.api.champion;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "base_stats")
public class BaseStat implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String value;

    protected BaseStat() {

    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

}