package org.bitbucket.deividasp.leaguewiki.controller;

import org.bitbucket.deividasp.leaguewiki.model.api.champion.Champion;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.ChampionRole;
import org.bitbucket.deividasp.leaguewiki.service.IWikiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ChampionController {

    @Autowired
    private IWikiService service;

    @RequestMapping(value = "/api/get/champions")
    public List<Champion> getChampions() {
        return service.getChampions();
    }

    @RequestMapping(value = "/api/get/champions/id/{id}")
    public Champion getChampion(@PathVariable long id) {
        return service.getChampion(id);
    }

    @RequestMapping(value = "/api/get/champions/name/{name}")
    public Champion getChampion(@PathVariable String name) {
        return service.getChampion(name);
    }

    @RequestMapping(value = "/api/get/champions/roles")
    public List<ChampionRole> getChampionRoles() {
        return Stream.of(ChampionRole.values()).collect(Collectors.toList());
    }

    @RequestMapping(value = "/api/get/champions/role/{roleName}")
    public List<Champion> getChampionsByRole(@PathVariable String roleName) {
        return service.getChampionsByRole(roleName);
    }

    @RequestMapping(value = "/api/get/champions/primary_role/{roleName}")
    public List<Champion> getChampionsByPrimaryRole(@PathVariable String roleName) {
        return service.getChampionsByPrimaryRole(roleName);
    }

    @RequestMapping(value = "/api/get/champions/secondary_role/{roleName}")
    public List<Champion> getChampionsBySecondaryRole(@PathVariable String roleName) {
        return service.getChampionsBySecondaryRole(roleName);
    }
    
	@RequestMapping(value = "/api/get/champions/names")
	public List<String> getNames() {
		return service.getChampionNames();
	}

    @RequestMapping(value = "/api/submit/champion", method = RequestMethod.POST)
    public void submitChampion(@RequestBody Champion champion) {
        service.submitChampion(champion);
    }

    @RequestMapping(value = "/api/submitted/champions")
    public List<Champion> getSubmittedChampions() {
        return service.getSubmittedChampions();
    }

    @RequestMapping(value = "/api/accept/champions/{id}")
    public void acceptChampion(@PathVariable long id) {
        service.acceptChampion(id);
    }

    @RequestMapping(value = "/api/remove/champions/{id}")
    public void removeChampion(@PathVariable long id) {
        service.removeChampion(id);
    }

	@RequestMapping(value = {"/api/get/champions/pages/{championsPerPage}", "/api/get/champions/pages/{championsPerPage}/{nameFilter}"})
	public int getPages(@PathVariable int championsPerPage, @PathVariable Optional<String> nameFilter) {
		List<Champion> allChampions;

		if (nameFilter.isPresent()) {
			allChampions = getChampions().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allChampions = getChampions();
		}

		return (int) Math.ceil(allChampions.size() / (double) championsPerPage);
	}

	@RequestMapping(value = {"/api/get/champions/page/{championsPerPage}/{page}", "/api/get/champions/page/{championsPerPage}/{page}/{nameFilter}"})
	public List<Champion> getPage(@PathVariable int championsPerPage, @PathVariable int page, @PathVariable Optional<String> nameFilter) {
		List<Champion> allChampions;

		if (nameFilter.isPresent()) {
			allChampions = getChampions().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allChampions = getChampions();
		}

		List<Champion> pageChampions = new ArrayList<>();

		for (int i = championsPerPage * (page - 1); i < championsPerPage * page; i++) {
			if (i > allChampions.size() - 1) {
				break;
			}

			pageChampions.add(allChampions.get(i));
		}

		return pageChampions;
	}

	@RequestMapping(value = {"/api/submitted/champions/pages/{championsPerPage}", "/api/submitted/champions/pages/{championsPerPage}/{nameFilter}"})
	public int getSubmittedPages(@PathVariable int championsPerPage, @PathVariable Optional<String> nameFilter) {
		List<Champion> allChampions;

		if (nameFilter.isPresent()) {
			allChampions = getSubmittedChampions().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allChampions = getSubmittedChampions();
		}

		return (int) Math.ceil(allChampions.size() / (double) championsPerPage);
	}

	@RequestMapping(value = {"/api/submitted/champions/page/{championsPerPage}/{page}", "/api/submitted/champions/page/{championsPerPage}/{page}/{nameFilter}"})
	public List<Champion> getSubmittedPage(@PathVariable int championsPerPage, @PathVariable int page, @PathVariable Optional<String> nameFilter) {
		List<Champion> allChampions;

		if (nameFilter.isPresent()) {
			allChampions = getSubmittedChampions().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allChampions = getSubmittedChampions();
		}

		List<Champion> pageChampions = new ArrayList<>();

		for (int i = championsPerPage * (page - 1); i < championsPerPage * page; i++) {
			if (i > allChampions.size() - 1) {
				break;
			}

			pageChampions.add(allChampions.get(i));
		}

		return pageChampions;
	}

}