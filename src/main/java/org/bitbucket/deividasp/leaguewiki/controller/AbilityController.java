package org.bitbucket.deividasp.leaguewiki.controller;

import org.bitbucket.deividasp.leaguewiki.model.api.ability.Ability;
import org.bitbucket.deividasp.leaguewiki.service.IWikiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class AbilityController {

    @Autowired
    private IWikiService service;

    @RequestMapping(value = "/api/get/abilities")
    public List<Ability> getAbilities() {
        return service.getAbilities();
    }

    @RequestMapping(value = "/api/get/abilities/id/{id}")
    public Ability getAbility(@PathVariable long id) {
        return service.getAbility(id);
    }

    @RequestMapping(value = "/api/get/abilities/name/{name}")
    public Ability getAbility(@PathVariable String name) {
        return service.getAbility(name);
    }

    @RequestMapping(value = "/api/submit/ability", method = RequestMethod.POST)
    public void submitAbility(@RequestBody Ability ability) {
        service.submitAbility(ability);
    }

    @RequestMapping(value = "/api/submitted/abilities")
    public List<Ability> getSubmittedAbilities() {
        return service.getSubmittedAbilities();
    }

    @RequestMapping(value = "/api/accept/abilities/{id}")
    public void acceptAbility(@PathVariable long id) {
        service.acceptAbility(id);
    }

    @RequestMapping(value = "/api/remove/abilities/{id}")
    public void removeAbility(@PathVariable long id) {
        service.removeAbility(id);
    }

	@RequestMapping(value = {"/api/get/abilities/pages/{abilitiesPerPage}", "/api/get/abilities/pages/{abilitiesPerPage}/{nameFilter}"})
	public int getPages(@PathVariable int abilitiesPerPage, @PathVariable Optional<String> nameFilter) {
		List<Ability> allAbilities;

		if (nameFilter.isPresent()) {
			allAbilities = getAbilities().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allAbilities = getAbilities();
		}

		return (int) Math.ceil(allAbilities.size() / (double) abilitiesPerPage);
	}

	@RequestMapping(value = {"/api/get/abilities/page/{abilitiesPerPage}/{page}", "/api/get/abilities/page/{abilitiesPerPage}/{page}/{nameFilter}"})
	public List<Ability> getPage(@PathVariable int abilitiesPerPage, @PathVariable int page, @PathVariable Optional<String> nameFilter) {
		List<Ability> allAbilities;

		if (nameFilter.isPresent()) {
			allAbilities = getAbilities().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allAbilities = getAbilities();
		}

		List<Ability> pageAbilities = new ArrayList<>();

		for (int i = abilitiesPerPage * (page - 1); i < abilitiesPerPage * page; i++) {
			if (i > allAbilities.size() - 1) {
				break;
			}

			pageAbilities.add(allAbilities.get(i));
		}

		return pageAbilities;
	}

	@RequestMapping(value = {"/api/submitted/abilities/pages/{abilitiesPerPage}", "/api/submitted/abilities/pages/{abilitiesPerPage}/{nameFilter}"})
	public int getSubmittedPages(@PathVariable int abilitiesPerPage, @PathVariable Optional<String> nameFilter) {
		List<Ability> allAbilities;

		if (nameFilter.isPresent()) {
			allAbilities = getSubmittedAbilities().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allAbilities = getSubmittedAbilities();
		}

		return (int) Math.ceil(allAbilities.size() / (double) abilitiesPerPage);
	}

	@RequestMapping(value = {"/api/submitted/abilities/page/{abilitiesPerPage}/{page}", "/api/submitted/abilities/page/{abilitiesPerPage}/{page}/{nameFilter}"})
	public List<Ability> getSubmittedPage(@PathVariable int abilitiesPerPage, @PathVariable int page, @PathVariable Optional<String> nameFilter) {
		List<Ability> allAbilities;

		if (nameFilter.isPresent()) {
			allAbilities = getSubmittedAbilities().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allAbilities = getSubmittedAbilities();
		}

		List<Ability> pageAbilities = new ArrayList<>();

		for (int i = abilitiesPerPage * (page - 1); i < abilitiesPerPage * page; i++) {
			if (i > allAbilities.size() - 1) {
				break;
			}

			pageAbilities.add(allAbilities.get(i));
		}

		return pageAbilities;
	}

}