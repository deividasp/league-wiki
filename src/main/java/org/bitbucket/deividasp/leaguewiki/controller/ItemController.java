package org.bitbucket.deividasp.leaguewiki.controller;

import org.bitbucket.deividasp.leaguewiki.model.api.item.Item;
import org.bitbucket.deividasp.leaguewiki.model.api.item.effect.UniqueEffectType;
import org.bitbucket.deividasp.leaguewiki.service.IWikiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ItemController {

    @Autowired
    private IWikiService service;

    @RequestMapping(value = "/api/get/items")
    public List<Item> getItems() {
        return service.getItems();
    }

    @RequestMapping(value = "/api/get/items/id/{id}")
    public Item getItem(@PathVariable long id) {
        return service.getItem(id);
    }

    @RequestMapping(value = "/api/get/items/name/{name}")
    public Item getItem(@PathVariable String name) {
        return service.getItem(name);
    }

    @RequestMapping(value = "/api/submit/item", method = RequestMethod.POST)
    public void submitItem(@RequestBody Item item) {
        service.submitItem(item);
    }

    @RequestMapping(value = "/api/submitted/items")
    public List<Item> getSubmittedItems() {
        return service.getSubmittedItems();
    }

    @RequestMapping(value = "/api/accept/items/{id}")
    public void acceptItem(@PathVariable long id) {
        service.acceptItem(id);
    }

    @RequestMapping(value = "/api/remove/items/{id}")
    public void removeItem(@PathVariable long id) {
        service.removeItem(id);
    }

	@RequestMapping(value = "/api/get/items/unique_effect_types")
	public List<UniqueEffectType> getUniqueEffectTypes() {
		return Stream.of(UniqueEffectType.values()).collect(Collectors.toList());
	}

	@RequestMapping(value = {"/api/get/items/pages/{itemsPerPage}", "/api/get/items/pages/{itemsPerPage}/{nameFilter}"})
	public int getPages(@PathVariable int itemsPerPage, @PathVariable Optional<String> nameFilter) {
		List<Item> allItems;

		if (nameFilter.isPresent()) {
			allItems = getItems().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allItems = getItems();
		}

		return (int) Math.ceil(allItems.size() / (double) itemsPerPage);
	}

	@RequestMapping(value = {"/api/get/items/page/{itemsPerPage}/{page}", "/api/get/items/page/{itemsPerPage}/{page}/{nameFilter}"})
	public List<Item> getPage(@PathVariable int itemsPerPage, @PathVariable int page, @PathVariable Optional<String> nameFilter) {
		List<Item> allItems;

		if (nameFilter.isPresent()) {
			allItems = getItems().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allItems = getItems();
		}

		List<Item> pageItems = new ArrayList<>();

		for (int i = itemsPerPage * (page - 1); i < itemsPerPage * page; i++) {
			if (i > allItems.size() - 1) {
				break;
			}

			pageItems.add(allItems.get(i));
		}

		return pageItems;
	}

	@RequestMapping(value = {"/api/submitted/items/pages/{itemsPerPage}", "/api/submitted/items/pages/{itemsPerPage}/{nameFilter}"})
	public int getSubmittedPages(@PathVariable int itemsPerPage, @PathVariable Optional<String> nameFilter) {
		List<Item> allItems;

		if (nameFilter.isPresent()) {
			allItems = getSubmittedItems().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allItems = getSubmittedItems();
		}

		return (int) Math.ceil(allItems.size() / (double) itemsPerPage);
	}

	@RequestMapping(value = {"/api/submitted/items/page/{itemsPerPage}/{page}", "/api/submitted/items/page/{itemsPerPage}/{page}/{nameFilter}"})
	public List<Item> getSubmittedPage(@PathVariable int itemsPerPage, @PathVariable int page, @PathVariable Optional<String> nameFilter) {
		List<Item> allItems;

		if (nameFilter.isPresent()) {
			allItems = getSubmittedItems().stream().filter(a -> a.getName().toLowerCase().contains(nameFilter.get().toLowerCase())).collect(Collectors.toList());
		} else {
			allItems = getSubmittedItems();
		}

		List<Item> pageItems = new ArrayList<>();

		for (int i = itemsPerPage * (page - 1); i < itemsPerPage * page; i++) {
			if (i > allItems.size() - 1) {
				break;
			}

			pageItems.add(allItems.get(i));
		}

		return pageItems;
	}

}