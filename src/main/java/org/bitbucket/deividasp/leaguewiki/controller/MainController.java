package org.bitbucket.deividasp.leaguewiki.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

	@RequestMapping("/")
	public String index() {
		return "/views/index.html";
	}

	@RequestMapping("/submit/champion")
	public String championSubmit() {
		return "/views/champion/submit.html";
	}

	@RequestMapping("/submit/ability")
	public String abilitySubmit() {
		return "/views/ability/submit.html";
	}

	@RequestMapping("/submit/item")
	public String itemSubmit() {
		return "/views/item/submit.html";
	}

	@RequestMapping("/admincp")
	public String admincp() {
		return "/views/admin/admincp.html";
	}

	@RequestMapping("/items/view")
	public String itemView() {
		return "/views/item/view.html";
	}

	@RequestMapping("/items")
	public String items() {
		return "/views/item/list.html";
	}

	@RequestMapping("/admin/submitted/items")
	public String adminSubmittedItems() {
		return "/views/admin/submitted_items.html";
	}

	@RequestMapping("/admin/accepted/items")
	public String adminAcceptedItems() {
		return "/views/admin/accepted_items.html";
	}

	@RequestMapping("/admin/accepted/items/view")
	public String adminAcceptedItemView() {
		return "/views/admin/view/accepted_item_view.html";
	}

	@RequestMapping("/admin/submitted/items/view")
	public String adminSubmittedItemView() {
		return "/views/admin/view/submitted_item_view.html";
	}

	@RequestMapping("/abilities/view")
	public String abilityView() {
		return "/views/ability/view.html";
	}

	@RequestMapping("/abilities")
	public String abilities() {
		return "/views/ability/list.html";
	}

	@RequestMapping("/admin/submitted/abilities")
	public String adminSubmittedAbilities() {
		return "/views/admin/submitted_abilities.html";
	}

	@RequestMapping("/admin/accepted/abilities")
	public String adminAcceptedAbilities() {
		return "/views/admin/accepted_abilities.html";
	}

	@RequestMapping("/admin/submitted/abilities/view")
	public String adminSubmittedAbilityView() {
		return "/views/admin/view/submitted_ability_view.html";
	}

	@RequestMapping("/admin/accepted/abilities/view")
	public String adminAcceptedAbilityView() {
		return "/views/admin/view/accepted_ability_view.html";
	}

	@RequestMapping("/champions/view")
	public String championView() {
		return "/views/champion/view.html";
	}

	@RequestMapping("/champions")
	public String champions() {
		return "/views/champion/list.html";
	}

	@RequestMapping("/admin/submitted/champions")
	public String adminSubmittedChampions() {
		return "/views/admin/submitted_champions.html";
	}

	@RequestMapping("/admin/accepted/champions")
	public String adminAcceptedChampions() {
		return "/views/admin/accepted_champions.html";
	}

	@RequestMapping("/admin/accepted/champions/view")
	public String adminAcceptedChampionView() {
		return "/views/admin/view/accepted_champion_view.html";
	}

	@RequestMapping("/admin/submitted/champions/view")
	public String adminSubmittedChampionView() {
		return "/views/admin/view/submitted_champion_view.html";
	}

	@RequestMapping("/submitted")
	public String submitted() {
		return "/views/submitted.html";
	}

}