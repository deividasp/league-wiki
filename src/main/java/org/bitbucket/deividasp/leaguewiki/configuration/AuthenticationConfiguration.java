package org.bitbucket.deividasp.leaguewiki.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

@Configuration
@ComponentScan("org.bitbucket.deividasp.leaguewiki")
@EnableWebSecurity
public class AuthenticationConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
        authBuilder.inMemoryAuthentication().withUser("admin").password("admin").authorities("ADMIN");
    }

    private BasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
        BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
        entryPoint.setRealmName("LeagueWiki Realm");
        return entryPoint;
    }

    @Override
    protected void configure(HttpSecurity security) throws Exception {
        security.authorizeRequests()
		        .antMatchers("/admin/**").hasAuthority("ADMIN")
		        .antMatchers("/admincp").hasAuthority("ADMIN")
                .antMatchers("/api/accept/**").hasAuthority("ADMIN")
                .antMatchers("/api/remove/**").hasAuthority("ADMIN")
                .antMatchers("/api/submitted/**").hasAuthority("ADMIN")
                .and().formLogin().permitAll()
                .and().httpBasic().authenticationEntryPoint(getBasicAuthEntryPoint())
                .and().csrf().disable();
    }

}