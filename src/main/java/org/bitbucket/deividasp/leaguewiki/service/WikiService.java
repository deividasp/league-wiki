package org.bitbucket.deividasp.leaguewiki.service;

import org.bitbucket.deividasp.leaguewiki.model.api.ability.Ability;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.Champion;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.ChampionRole;
import org.bitbucket.deividasp.leaguewiki.model.api.item.Item;
import org.bitbucket.deividasp.leaguewiki.repository.ability.AbilityRepository;
import org.bitbucket.deividasp.leaguewiki.repository.champion.ChampionRepository;

import org.bitbucket.deividasp.leaguewiki.repository.item.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WikiService implements IWikiService {

    @Autowired
    private ChampionRepository championRepository;

    @Autowired
    private AbilityRepository abilityRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public List<Champion> getChampions() {
        return championRepository.getAll();
    }

    @Override
    public Champion getChampion(long id) {
        return championRepository.get(id);
    }

    @Override
    public Champion getChampion(String name) {
        return championRepository.get(name);
    }

    @Override
    public List<ChampionRole> getChampionRoles() {
        return championRepository.getRoles();
    }

    @Override
    public List<Champion> getChampionsByRole(String roleName) {
        return championRepository.getByRole(roleName);
    }

    @Override
    public List<Champion> getChampionsByPrimaryRole(String roleName) {
        return championRepository.getByPrimaryRole(roleName);
    }

    @Override
    public List<Champion> getChampionsBySecondaryRole(String roleName) {
        return championRepository.getBySecondaryRole(roleName);
    }

	@Override
	public List<String> getChampionNames() {
		return championRepository.getNames();
	}

    @Override
    public void submitChampion(Champion champion) {
        championRepository.submit(champion);
    }

    @Override
    public List<Champion> getSubmittedChampions() {
        return championRepository.getSubmitted();
    }

    @Override
    public void acceptChampion(long id) {
        championRepository.accept(id);
    }

    @Override
    public void removeChampion(long id) {
        championRepository.remove(id);
    }

	@Override
    public List<Ability> getAbilities() {
        return abilityRepository.getAll();
    }

    @Override
    public Ability getAbility(long id) {
        return abilityRepository.get(id);
    }

    @Override
    public Ability getAbility(String name) {
        return abilityRepository.get(name);
    }

    @Override
    public void submitAbility(Ability ability) {
        abilityRepository.submit(ability);
    }

    @Override
    public List<Ability> getSubmittedAbilities() {
        return abilityRepository.getSubmitted();
    }

    @Override
    public void acceptAbility(long id) {
        abilityRepository.accept(id);
    }

    @Override
    public void removeAbility(long id) {
        abilityRepository.remove(id);
    }

    @Override
    public List<Item> getItems() {
        return itemRepository.getAll();
    }

    @Override
    public Item getItem(long id) {
        return itemRepository.get(id);
    }

    @Override
    public Item getItem(String name) {
        return itemRepository.get(name);
    }

    @Override
    public void submitItem(Item item) {
        itemRepository.submit(item);
    }

    @Override
    public List<Item> getSubmittedItems() {
        return itemRepository.getSubmitted();
    }

    @Override
    public void acceptItem(long id) {
        itemRepository.accept(id);
    }

    @Override
    public void removeItem(long id) {
        itemRepository.remove(id);
    }

}