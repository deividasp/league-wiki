package org.bitbucket.deividasp.leaguewiki.service;

import org.bitbucket.deividasp.leaguewiki.model.api.ability.Ability;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.Champion;
import org.bitbucket.deividasp.leaguewiki.model.api.champion.ChampionRole;
import org.bitbucket.deividasp.leaguewiki.model.api.item.Item;

import java.util.List;

public interface IWikiService {

    /*
     * Champions.
     */

    List<Champion> getChampions();

    Champion getChampion(long id);
    Champion getChampion(String name);

    List<ChampionRole> getChampionRoles();

    List<Champion> getChampionsByRole(String roleName);
    List<Champion> getChampionsByPrimaryRole(String roleName);
    List<Champion> getChampionsBySecondaryRole(String roleName);

	List<String> getChampionNames();

    void submitChampion(Champion champion);

    List<Champion> getSubmittedChampions();

    void acceptChampion(long id);
    void removeChampion(long id);

    /*
     * Abilities.
     */

    List<Ability> getAbilities();

    Ability getAbility(long id);
    Ability getAbility(String name);

    void submitAbility(Ability ability);

    List<Ability> getSubmittedAbilities();

    void acceptAbility(long id);
    void removeAbility(long id);

    /*
     * Items.
     */

    List<Item> getItems();

    Item getItem(long id);
    Item getItem(String name);

    void submitItem(Item item);

    List<Item> getSubmittedItems();

    void acceptItem(long id);
    void removeItem(long id);

}