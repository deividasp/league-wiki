$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
}

$(document).ready(function() {
    var itemId = parseInt($.urlParam("id"));

    $.ajax({
        url: '/api/get/items/id/' + itemId
    }).then(function(data) {
        var name = data.name;
        var cost = data.cost;
        var bonuses = data.bonuses;
        var effects = data.effects;

        $('#name').text(name);
        $('#cost').text(cost);

        var bonusesText = '';
        var effectsText = '';

        for (var i = 0; i < bonuses.length; i++) {
            bonusesText += bonuses[i].name + ': ';
            bonusesText += bonuses[i].bonus + '<br />';
        }

        for (var i = 0; i < effects.length; i++) {
            effectsText += effects[i].type + ': ';
            effectsText += effects[i].description + '<br />';
        }

        $('#bonuses').html(bonusesText);
        $('#effects').html(effectsText);
    });

    $('#back').click(function() {
        window.location.href = '/items?page=1';
    });
});