$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
}

var abilitiesPerPage = 10;

$(document).ready(function() {
    var currentPage = parseInt($.urlParam("page"));

    var filter;
    var applyFilter = true;

    try {
        filter = $.urlParam("filter");
    } catch (e) {
        applyFilter = false;
    }

    $.ajax({
        url: '/api/submitted/abilities/pages/' + abilitiesPerPage + (applyFilter ? ('/' + filter) : '')
    }).then(function(data) {
        var maxPage = data;

        $.ajax({
            url: '/api/submitted/abilities/page/' + abilitiesPerPage + '/' + currentPage + (applyFilter ? ('/' + filter) : '')
        }).then(function(data) {
            if (data.length >= 1) {
                $('#ability-table-row-1').remove();
            }

            for (var i = 0; i < data.length; i++) {
                if (i == 0) {
                    $('#ability-table > tbody').append('<tr id="ability-table-row-' + (i + 1) + '"><td>' + data[i].name + '</td><td><a href="/admin/submitted/abilities/view?id=' + data[i].id + '"><span class="glyphicon glyphicon-eye-open"></span></a></td></tr>');
                } else {
                    $('#ability-table > tbody > tr:last').after('<tr id="ability-table-row-' + (i + 1) + '"><td>' + data[i].name + '</td><td><a href="/admin/submitted/abilities/view?id=' + data[i].id + '"><span class="glyphicon glyphicon-eye-open"></span></a></td></tr>');
                }
            }

            if (currentPage > 1) {
                $('table').after('<a id="previous-page" class="prv btn btn-success pull-left"><span class="glyphicon glyphicon glyphicon-arrow-left"></span></a>');
            }

            if (maxPage <= currentPage) {
                return;
            }

            if (currentPage > 1) {
                $('#previous-page').after('<a id="next-page" class="nxt btn btn-success pull-right"><span class="glyphicon glyphicon-arrow-right"></span></a>');
            } else if (currentPage <= 1) {
                $('table').after('<a id="next-page" class="nxt btn btn-success pull-right"><span class="glyphicon glyphicon-arrow-right"></span></a>');
            }
        });
    });

    $(document).on('click', '.prv', function() {
        window.location.href = '/admin/submitted/abilities?page=' + (currentPage - 1) + (applyFilter ? ('&filter=' + filter) : '');
    });

    $(document).on('click', '.nxt', function() {
        window.location.href = '/admin/submitted/abilities?page=' + (currentPage + 1) + (applyFilter ? ('&filter=' + filter) : '');
    });

    $('#search-button').click(function() {
        var searchValue = $('#search-field').val();

        if (searchValue.length > 0) {
            window.location.href = '/admin/submitted/abilities?page=1&filter=' + searchValue;
        } else {
            window.location.href = '/admin/submitted/abilities?page=1';
        }
    });
});