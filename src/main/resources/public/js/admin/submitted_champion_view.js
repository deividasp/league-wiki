$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
}

$(document).ready(function() {
    var championId = parseInt($.urlParam("id"));

    $.ajax({
        url: '/api/get/champions/id/' + championId
    }).then(function(data) {
        var name = data.name;
        var description = data.description;
        var baseStats = data.baseStats;
        var primaryRole = data.primaryRole;
        var secondaryRole = data.secondaryRole;

        var text = '';

        for (var i = 0; i < baseStats.length; i++) {
            text += baseStats[i].name + ': ';
            text += baseStats[i].value + '<br />';
        }

        $('#name').text(name);
        $('#description').text(description);
        $('#base-stats').html(text);
        $('#primary-role').text(primaryRole);
        $('#secondary-role').text(secondaryRole);
    });

    $('#decline').click(function() {
        $.ajax({
            url: '/api/remove/champions/' + championId
        }).then(function() {
            window.location.href = '/admin/submitted/champions?page=1';
        });
    });

    $('#accept').click(function() {
        $.ajax({
            url: '/api/accept/champions/' + championId
        }).then(function() {
            window.location.href = '/admin/submitted/champions?page=1';
        });
    });
});