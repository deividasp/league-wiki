$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
}

$(document).ready(function() {
    var abilityId = parseInt($.urlParam("id"));

    $.ajax({
        url: '/api/get/abilities/id/' + abilityId
    }).then(function(data) {
        var name = data.name;
        var description = data.description;
        var cost = data.cost;
        var castRange = data.castRange;

        $('#name').text(name);
        $('#description').text(description);

        $.ajax({
            url: '/api/get/champions/id/' + data.championId,

            success: function(data) {
                $('#champion').text(data.name);
            },

            error: function() {
                $('#champion').text('N/A');
            }
        });

        $('#cost').text(cost);
        $('#cast-range').text(castRange);
    });

    $('#back').click(function() {
        window.location.href = '/admin/accepted/abilities?page=1';
    });

    $('#remove').click(function() {
        $.ajax({
            url: '/api/remove/abilities/' + abilityId
        }).then(function() {
            window.location.href = '/admin/accepted/abilities?page=1';
        });
    });
});