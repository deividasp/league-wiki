$(document).ready(function() {
    $.ajax({
        url: '/api/get/champions/names'
    }).then(function(data) {
        $.each(data, function(key, value) {
            $('#champion-select').append($('<option/>', {
                value: key,
                text: value
            }));
        });
    });

    $('#submit-button').click(function() {
        if ($('#submit-button').hasClass('disabled')) {
            return;
        }

        var name = $('#name-input').val();
        var description = $('#description-input').val();
        var championName = $('#champion-select option:selected').text();
        var championId;
        var cost = $('#cost-input').val();
        var castRange = $('#cast-range-input').val();

        $.ajax({
            url: '/api/get/champions/name/' + championName,
            
            success: function(data) {
                championId = data.id;

                $.ajax({
                    type: "POST",
                    url: '/api/submit/ability',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        championId: championId,
                        name: name,
                        description: description,
                        cost: cost,
                        castRange: castRange
                    }),

                    success: function() {
                        window.location.href = '/submitted?type=ability';
                    },

                    error: function() {
                        window.location.href = '/error?type=ability&action=submit'
                    }
                })
            },

            error: function(data) {
                window.location.href = '/error?type=ability&action=submit';
            }
        });
    });
});