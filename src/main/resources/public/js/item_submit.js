$(document).ready(function() {
    var i1 = 1;
    var i2 = 1;

    $.ajax({
        url: '/api/get/items/unique_effect_types'
    }).then(function(data) {
        $.each(data, function(key, value) {
            $('#effect-type-select-1').append($('<option/>', {
                value: key,
                text: value
            }));
        });
    });

    $('#bonuses-row-add').click(function() {
        i1++;
        $('#bonuses-table > tbody > tr:last').after('<tr id="bonuses-table-row-' + i1 + '"><td><input type="text" name="name-' + i1 + '" class="form-control" /></td><td><input type="text" name="value-' + i1 + '" class="form-control" /</td></tr>');
    });

    $('#bonuses-row-delete').click(function() {
        if (i1 > 1) {
            $('#bonuses-table-row-' + i1--).remove();
        }
    });

    $('#effects-row-add').click(function() {
        i2++;
        $('#effects-table > tbody > tr:last').after('<tr id="effects-table-row-' + i2 + '"><td><select class="form-control" id="effect-type-select-' + i2 + '"</select></td><td><input type="text" name="value-' + i2 + '" class="form-control" /</td></tr>');

        $.ajax({
            url: '/api/get/items/unique_effect_types'
        }).then(function(data) {
            $.each(data, function(key, value) {
                $('#effect-type-select-'  + i2).append($('<option/>', {
                    value: key,
                    text: value
                }));
            });
        });
    });

    $('#effects-row-delete').click(function() {
        if (i2 > 1) {
            $('#effects-table-row-' + i2--).remove();
        }
    });

    $('#submit-button').click(function() {
        if ($('#submit-button').hasClass('disabled')) {
            return;
        }

        var name = $('#name-input').val();
        var cost = $('#cost-input').val();

        var bonuses = [];
        var row1 = 0;

        var effects = [];
        var row2 = 0;

        $('#bonuses-table tbody').find('tr').each(function () {
            var $tds = $(this).find('input'),
                name = $tds.eq(0).val(),
                bonus = $tds.eq(1).val();

            if (name.length > 0 && bonus.length > 0) {
                bonuses[row1] = {};
                bonuses[row1]['name'] = name;
                bonuses[row1++]['bonus'] = bonus;
            }
        });

        $('#effects-table tbody').find('tr').each(function () {
            var $select = $(this).find('select option:selected');
            var $input = $(this).find('input');

            var type = $select.eq(0).text();
            var description = $input.eq(0).val();

            if (description.length > 0) {
                effects[row2] = {};
                effects[row2]['type'] = type;
                effects[row2++]['description'] = description;
            }
        });

        $.ajax({
            type: "POST",
            url: '/api/submit/item',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                name: name,
                cost: cost,
                bonuses: bonuses,
                effects: effects
            }),

            success: function() {
                window.location.href = '/submitted?type=item';
            },

            error: function() {
                window.location.href = '/error?type=item&action=submit'
            }
        });
    });
});