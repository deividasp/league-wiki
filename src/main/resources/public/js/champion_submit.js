$(document).ready(function() {
    var i = 1;

    $.ajax({
        url: '/api/get/champions/roles'
    }).then(function(data) {
        $.each(data, function(key, value) {
            $('#primary-role-select').append($('<option/>', {
                value: key,
                text: value
            }));
            $('#secondary-role-select').append($('<option/>', {
                value: key,
                text: value
            }));
        });
    });

    $('#base-stats-row-add').click(function() {
        i++;
        $('#base-stats-table > tbody > tr:last').after('<tr id="base-stats-table-row-' + i + '"><td><input type="text" name="name-' + i + '" class="form-control" /></td><td><input type="text" name="value-' + i + '" class="form-control" /</td></tr>');
    });

    $('#base-stats-row-delete').click(function() {
        if (i > 1) {
            $('#base-stats-table-row-' + i--).remove();
        }
    });

    $('#submit-button').click(function() {
        if ($('#submit-button').hasClass('disabled')) {
            return;
        }

        var name = $('#name-input').val();
        var description = $('#description-input').val();
        var primaryRole = $('#primary-role-select option:selected').text();
        var secondaryRole = $('#secondary-role-select option:selected').text();

        var baseStats = [];
        var row = 0;

        $('tbody').find('tr').each(function () {
            var $tds = $(this).find('input'),
                name = $tds.eq(0).val(),
                value = $tds.eq(1).val();

            if (name.length > 0 && value.length > 0) {
                baseStats[row] = {};
                baseStats[row]['name'] = name;
                baseStats[row++]['value'] = value;
            }
        });

        $.ajax({
            type: "POST",
            url: '/api/submit/champion',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                name: name,
                description: description,
                primaryRole: primaryRole,
                secondaryRole: secondaryRole,
                baseStats: baseStats
            }),

            success: function() {
                window.location.href = '/submitted?type=champion';
            },

            error: function() {
                window.location.href = '/error?type=champion&action=submit'
            }
        });
    });
});