$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
}

$(document).ready(function() {
    var type = $.urlParam('type');
    var text = 'Your ' + type + ' has been submitted';

    $('#title-text').text(text);
});